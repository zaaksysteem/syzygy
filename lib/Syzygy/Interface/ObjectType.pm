package Syzygy::Interface::ObjectType;
our $VERSION = '0.006';
use Moose::Role;
use namespace::autoclean;

=head1 NAME

Syzygy::Interface::ObjectType - Common API for L<Syzygy::Object> types.

=head1 DESCRIPTION

This interface spdecifies the required methods for object type instances.

=cut

use BTTW::Tools;
use Syzygy::Types qw[Name ObjectAttribute];

=head1 REQUIRED INTERFACES

=head2 name

    => Name

Requires a static name method.

=head2 has_atribute

    Name => Bool

Must return true/false on the existance predicate of an attribute, by name.

=head2 get_attribute

    Name => ?Syzygy::Object::Attribute

Must return either a L<Syzygy::Object::Attribute> instance, or C<undef>.

=head2 set_attribute

    Name, Syzygy::Object::Attribute =>

Set a L<Syzygy::Object::Attribute> by it's name.

=head2 all_attributes

    => @Syzygy::Object::Attribute

Must return a list of all associated L<Syzygy::Object::Attribute> instances.

=head2 attribute_names

    => @Name

Must return a list of all names of associated L<Syzygy::Object::Attribute>
instances.

=head2 build_preview

    => Maybe[Str]

May return a string representing the object instance identity in
human-readable form.

=cut

requires qw[
    name

    has_attribute
    get_attribute
    set_attribute
    all_attributes
    attribute_names

    build_preview
];

#sig does not support MooseX::Types: sig name => '=> Name';
around name => sub {
    my $orig = shift;
    my $self = shift;

    my $maybe_name = $self->$orig(@_);

    Name->assert_valid($maybe_name);

    return $maybe_name;
};

sig has_attribute => '=> Bool';

sig get_attribute => '=> ?Syzygy::Object::Attribute';

#sig does not support MooseX::Types: sig set_attribute => 'Name, ObjectAttribute';
around set_attribute => sub {
    my $orig = shift;
    my $self = shift;

    my ($name, $attribute) = @_;

    Name->assert_valid($name);
    ObjectAttribute->assert_value($attribute);

    return $self->$orig($name, $attribute);
};

sig all_attributes => '=> @Syzygy::Object::Attribute';

#sig does not support MooseX::Types: sig attribute_names => '=> @Name';
around attribute_names => sub {
    my $orig = shift;
    my $self = shift;

    my @names = $self->$orig(@_);

    Name->assert_valid($_) for @names;

    return @names;
};

sig build_preview => 'Syzygy::Object::Instance => Maybe[Str]';

=head1 METHODS

=head2 process_attribute_values

This method takes a map of attribute-names to values and checks the data for
validity and completeness within the context of the type this method is
called on.

The returned value of this method can be assumed safe for instantiating a new
object of the given type.

If the provided value is a hard C<undef> (but the attribute name as key does
C<exists>), the value is ignored as 'not specified', and may cause an
exception if the attribute is required to have a value.

=cut

sig process_attribute_values => 'HashRef';

sub process_attribute_values {
    my $self = shift;
    my $values = shift;

    my %validation_results;
    my %processed_values;

    for my $attribute_name (keys %{ $values }) {
        unless ($self->has_attribute($attribute_name)) {
            # Not the type's place to determine if 'unknown' attribute values
            # consitute a problem or not. Just process what we can and return.
            next;
        }

        my $attribute = $self->get_attribute($attribute_name);

        # undefined values are silently ignored here, requiredness checking
        # is another step after this accumulation of values anyway.
        next unless defined $values->{ $attribute_name };

        my $value = try {
            return $attribute->value_type->new_value(
                $values->{ $attribute_name }
            );
        } catch {
            $validation_results{ $attribute_name } = {
                error => 'invalid_attribute',
                message => "$_"
            };

            return;
        };

        next unless defined $value;

        $processed_values{ $attribute_name } = $value;
    }

    if (scalar keys %validation_results) {
        throw('syzygy/object/type/instance_values_validation_error', sprintf(
            'Validation of instance values for type "%s" failed',
            $self->name
        ), \%validation_results);
    }

    return \%processed_values;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
