package Syzygy::Test::Object::ValueTypes::Boolean;

use Test::Class::Moose;
use Test::Fatal;

use Syzygy::Object::ValueTypes::Boolean;

sub test_object_value_type_boolean {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::Boolean->new }
        'boolean value type instantiates';

    isa_ok $type, 'Syzygy::Object::ValueTypes::Boolean',
        'boolean value type constructor retval';

    is $type->name, 'boolean', 'type has expected type name';

    my ($true, $false);

    lives_ok { $true = $type->new_value(1) } 'boolean value type can construct true value';
    lives_ok { $false = $type->new_value(0) } 'boolean value type can construct false value';

    is $true->type_name, 'boolean', 'true value has expected type name';
    is $false->type_name, 'boolean', 'false falue has expected type name';

    is $true->value, 1, 'true value has expected internal value';
    is $false->value, 0, 'false value has expected internal value';

    ok $type->equal($true, $true), 'type quality test true==true';
    ok $type->equal($false, $false), 'type quality test false==false';
    ok !$type->equal($true, $false), 'type non-equality test true!=false';
    ok !$type->equal($false, $true), 'type non-equality test false!=true';

    is_deeply $type->deflate_value($true), \1,
        'type true value deflation returns scalar-ref';

    is_deeply $type->deflate_value($false), \0,
        'type false value deflation returns scalar-ref';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
